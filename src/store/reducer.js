import { combineReducers } from 'redux'
import loyalty from '@@/reducer/loyaltyReducer'
import category from '@@/reducer/categoryReducer'
import common from './commonReducer'

const reducer = combineReducers({
  common,
  loyalty,
  category,
});

export default reducer;
