const currencyMap = {
  CNY: '¥',
  USD: '$',
  HKD: 'HK$',
  JPY: '¥',
  KRW: '₩',
  EUR: '€',
  GBP: '£',
  CHF: 'CHF',
  DKK: 'kr.',
  PLN: 'zł',
  RUB: '₽',
  SEK: 'kr',
  MXN: '$',
  BRL: 'R$',
  AUD: '$',
  CAD: '$',
  SGD: '$',
};

export default currencyMap;
