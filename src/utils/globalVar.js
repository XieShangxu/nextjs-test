// eslint-disable-next-line no-undef
const dev = process.env.NODE_ENV !== 'production';
export const PUBLIC_HOST = 'https://api.ff-svc.cn/westore';
export const INNER_HOST = 'https://dragon-channel-wechat-cn2.farfetch.net';

export const getBaseUrl = (isServer = false) => {
  if (dev) {
    return PUBLIC_HOST;
    // return 'https://dragon-channel-wechat-cn2.fftech.info';
  }
  // eslint-disable-next-line no-nested-ternary
  return isServer ? INNER_HOST : PUBLIC_HOST;
}
