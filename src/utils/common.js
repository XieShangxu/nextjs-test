export function getGUID() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = Math.random() * 16 | 0
    const v = c === 'x' ? r : (r & 0x3 | 0x8)

    return v.toString(16)
  });
}

export function toLocaleFixed(num, n) {
  return parseFloat(num).toLocaleString('en-GB', {
    minimumFractionDigits: n,
    maximumFractionDigits: n,
  });
}

export function formatTime(date) {
  const year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let hour = date.getHours();
  let minute = date.getMinutes();

  if (month < 10) { month = `0${month}`; }
  if (day < 10) { day = `0${day}`; }
  if (hour < 10) { hour = `0${hour}`; }
  if (minute < 10) { minute = `0${minute}`; }

  return `${year}年${month}月${day}日`;
}

export function getQueryString(search, name) {
  const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`, 'i');
  const value = search.substr(1).match(reg);

  if (value != null) { return (value[2]); }

  return null;
}
