export default function callAppFunction(apiName, params, callback = '') {
  params = JSON.stringify(params);

  try {
    window.webkit.messageHandlers.iOSPanda.postMessage({
      eventName: apiName,
      args: [
        params,
        callback,
      ],
    });
  } catch (e) {
    console.log(e);
  }
}
