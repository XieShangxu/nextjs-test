export function getProps(item) {
  const { type } = item || {};
  let props;
  let img;

  switch (type) {
    case 'image':
      [img] = (item.assets || []).filter((asset) => asset.size === 'Xs');
      props = img && img.source;
      break;
    case 'video':
      props = {
        provider: item.provider,
        source: item.source,
      };
      break;
    case 'link':
      props = item.target;
      break;
    case 'html':
      props = item.content;
      break;
    default:
      props = item.value;
  }

  return props;
}

function _getMedia(fields) {
  if (fields.video) {
    return { ...getProps(fields.video), imageUrl: getProps(fields.image) };
  }
  if (fields.image) {
    return getProps(fields.image);
  }

  return null;
}

export function convert(data, propName = 'components') {
  const props = data && data[propName];

  if (Array.isArray(props)) {
    return props.map((com) => {
      if (com.type !== 'list') {
        return convert(com, 'fields');
      }

      return null;
    });
  }
  if (props && typeof props === 'object') {
    const o = {};
    const customType = data && data.customType;

    Object.keys(props).forEach((key) => {
      const item = props[key];
      const {
        type,
        fields,
        displayOptions,
      } = item || {};

      if (type === 'list') {
        o[key] = convert(item);

        return;
      }
      if (fields) {
        o[key] = item.customType === 'media' ? _getMedia(fields) : convert(item, 'fields');

        return;
      }
      if (Object.keys(displayOptions).length) {
        o.displayOptions = displayOptions;
        o[key] = getProps(item);

        return;
      }
      o[key] = getProps(item);
    });

    return customType.indexOf('mod_') !== -1 ? { [customType.substring(4, customType.length)]: o } : o;
  }

  return null;
}

export function cmsParse(data) {
  const entries = (data || {}).entries || [];
  const obj = {};

  entries.forEach((entry) => {
    const { code } = entry;
    const propName = code;
    const tmpArr = [];

    convert(entry).forEach((o) => {
      tmpArr.push(o);
    });
    obj[propName] = tmpArr;
  });

  return obj;
}

export function parseMultiLine(text = '') {
  return text.split('\\n').map((txt) => txt.trim());
}
