import React from 'react';
import classNames from 'classnames';
import style from './spinner.less';

function getTemplate(type) {
  if (type === 'unready') {
    return (
      <div className={style['type-unready']} />
    );
  }
  if (type === 'processing') {
    return (
      <div className={style['type-processing']} />
    );
  }
  if (type === 'plp') {
    return (
      <div className={style['type-plp']}>
        <div className={style.list}>
          {
            [0, 1, 2, 3].map((index) => (
              <div key={index} className={style['list-item']}>
                <p className={classNames(style['img-block'], style['spinner-block'])} />
                <p className={classNames(style['desc-block-1'], style['spinner-block'])} />
                <p className={classNames(style['desc-block-2'], style['spinner-block'])} />
                <p className={classNames(style['desc-block-3'], style['spinner-block'])} />
              </div>
            ))
          }
        </div>
      </div>
    );
  }
  if (type === 'pdp') {
    return (
      <div className={style['type-pdp']}>
        <p className={classNames(style['img-block'], style['spinner-block'])} />
        <p className={classNames(style['desc-block-1'], style['spinner-block'])} />
        <p className={classNames(style['desc-block-2'], style['spinner-block'])} />
        <p className={classNames(style['desc-block-3'], style['spinner-block'])} />
        <p className={classNames(style['desc-block-4'], style['spinner-block'])} />
      </div>
    );
  }
  if (type === 'homepage') {
    return (
      <div className={style['type-homepage']}>
        <div className={style.header}>
          <p className={classNames(style['header-left'], style['spinner-block'])} />
          <p className={classNames(style['header-right'], style['spinner-block'])} />
        </div>
        <p className={classNames(style['banner-block'], style['spinner-block'])} />
        <p className={classNames(style['category-title'], style['spinner-block'])} />
        <p className={classNames(style['img-block'], style['spinner-block'])} />
        <div className={style['item-blocks']}>
          <p className={classNames(style['item-block-left'], style['spinner-block'])} />
          <p className={classNames(style['item-block-right'], style['spinner-block'])} />
        </div>
      </div>
    );
  }
  if (type === 'slideproducts') {
    return (
      <div className={style['type-slideproducts']}>
        <div className={style['slide-list']}>
          {
            [0, 1].map((index) => (
              <div key={index} className={style['list-item']}>
                <p className={classNames(style['img-block'], style['spinner-block'])} />
                <p className={classNames(style['desc-block-1'], style['spinner-block'])} />
                <p className={classNames(style['desc-block-2'], style['spinner-block'])} />
              </div>
            ))
          }
        </div>
      </div>
    );
  }

  return null;
}

const Spinner = ({ cls, type, full }) => {
  return (
    <div className={classNames(style['spinner-wrapper'], cls, full ? '' : style.part)}>
      {getTemplate(type)}
    </div>
  );
}

export default Spinner;
