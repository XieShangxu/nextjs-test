import { SET_LOYALTY_CONTENT } from '@@/store/constants'

const defaultState = {
  contentData: {},
  userAccessInfo: {},
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_LOYALTY_CONTENT:
      return {
        ...state,
        ...action.data,
      }
    default:
      return state;
  }
}
