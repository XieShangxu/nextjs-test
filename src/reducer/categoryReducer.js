import { SET_CATEGORY_CONTENT, SET_AZBRANDS } from '@@/store/constants'

const defaultState = {
  contentData: {},
  azBrands: {},
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case SET_CATEGORY_CONTENT:
      return {
        ...state,
        ...action.data,
      }
    case SET_AZBRANDS:
      return {
        ...state,
        azBrands: action.data,
      }
    default:
      return state;
  }
}
