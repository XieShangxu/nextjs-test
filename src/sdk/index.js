import axios from 'axios'
import { getBaseUrl } from '../utils/globalVar'
import { getGUID } from '../utils/common'

const clientInstance = axios.create({
  baseURL: getBaseUrl(false),
  timeout: 10000,
})

const serverInstance = axios.create({
  baseURL: getBaseUrl(true),
  timeout: 10000,
})

const setServerBaseUrl = (baseUrl) => {
  serverInstance.defaults.baseURL = baseUrl;
}

const instances = [clientInstance, serverInstance];

instances.map((instance) => {
  // 拦截器
  instance.interceptors.response.use((response) => {
    if (response.status === 200 || response.status === 202) {
      return response;
    }

    return Promise.reject(response)
  }, (error) => {
    return Promise.reject(error)
  })
  instance.interceptors.request.use((config) => {
    Object.assign(config.headers, {
      'Accept-Language': 'zh-CN',
      'X-SUMMER-RequestId': getGUID(),
      'Content-Type': 'application/json; charset=UTF-8',
      'FF-Country': 'CN',
      'FF-Currency': 'CNY',
      // 'X-WCS-Session-Key': '',
    });

    return config
  }, (error) => {
    return Promise.reject(error)
  })

  return instance
})

export {
  clientInstance,
  serverInstance,
  setServerBaseUrl,
};
