import { SET_LOYALTY_CONTENT } from '@@/store/constants'
import { cmsParse } from '@@/utils/cmsUtil'
import axiosF from 'axios'
import { serverInstance } from '../sdk'

const toObject = (arr) => {
  const obj = {};

  (arr || []).forEach((com) => {
    const [[key, value]] = Object.entries(com);
    obj[key] = value
  })

  return obj;
}

export const setLoyaltyContent = (data) => ({
  type: SET_LOYALTY_CONTENT,
  data,
})

export const getContent = (params, sessionKey = '') => async () => {
  const customParams = { params }
  if (sessionKey) {
    customParams.headers = { 'X-WCS-Session-Key': sessionKey }
  }
  const { data } = await serverInstance.get('/api/v2/search/contents', customParams);
  const content = cmsParse(data);

  // dispatch(setLoyaltyContent(toObject(content[params.codes])))

  return toObject(content[params.codes]);
}

export const getUserAccessInfo = (params) => async () => {
  const { data } = await axiosF.get(
    `https://channel-service-panda.farfetch.net/api/v0/marketing/v1/customers/${params.userId}/spendLevelPrograms/FFACCESS?currencyCode=${params.currency}`,
    {
      headers: {
        'X-SUMMER-RequestId': 1234,
        Authorization: `Bearer ${params.token}`,
      },
    },
  )
  console.log(data)

  return data;
}

export const initLoyaltyData = (pandaParams, contentParams) => async (dispatch) => {
  return Promise.all([
    dispatch(getUserAccessInfo(pandaParams)),
    dispatch(getContent(contentParams)),
  ]).then((data) => {
    return data
  }).catch((err) => {
    throw err
  })
}

export const test = () => {}
