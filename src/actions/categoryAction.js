import { clientInstance, serverInstance } from '../sdk'
import { SET_AZBRANDS } from '../store/constants'

export const getContent = (params, sessionKey = '') => async () => {
  const customParams = { params }
  if (sessionKey) {
    customParams.headers = { 'X-WCS-Session-Key': sessionKey }
  }
  const { data } = await serverInstance.get('/api/v2/search/contents', customParams);

  return data;
}

export const getAzBrands = (params) => async () => {
  const { data } = await clientInstance.get('/api/v1/menus/brands', params)

  return data;
}

export const setAzBrands = (data) => ({
  type: SET_AZBRANDS,
  data,
})
