import { clientInstance, serverInstance } from '../sdk'

export const getContent = (params, sessionKey = '') => async () => {
  const customParams = { params }
  if (sessionKey) {
    customParams.headers = { 'X-WCS-Session-Key': sessionKey }
  }
  const { data } = await serverInstance.get('/api/v2/search/contents', customParams);

  return data;
}

export const getListBySetId = (params, sessionKey = '') => async () => {
  const customParams = { params: params.param }
  if (sessionKey) {
    customParams.headers = { 'X-WCS-Session-Key': sessionKey }
  }
  const { data } = await clientInstance.get(`/api/v1/sets/${params.setId}/products`, customParams);

  return data;
}

export const getProductsBySearch = (params, sessionKey = '') => async () => {
  const customParams = { params }
  if (sessionKey) {
    customParams.headers = { 'X-WCS-Session-Key': sessionKey }
  }
  const { data } = await clientInstance.get('/api/v1/search/products', customParams);

  return data;
}

export const getProductById = (params, sessionKey = '') => async () => {
  const customParams = { params }
  if (sessionKey) {
    customParams.headers = { 'X-WCS-Session-Key': sessionKey }
  }

  const { data } = await clientInstance.get(`/api/v1/products/${params.id}`);

  return data;
}
