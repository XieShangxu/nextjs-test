import App from 'next/app'
import React from 'react'
import Head from 'next/head'
import { Provider } from 'react-redux'
import withRedux from 'next-redux-wrapper'
import { PUBLIC_HOST } from '@@/utils/globalVar'
import initStore from '../store'
import { setServerBaseUrl } from '../sdk'
import '../styles/common.less'
import '../styles/normalize.less'
import '../styles/font.less'

class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {}
    const { query: { ffdev } } = ctx.req

    if (ffdev === true) {
      setServerBaseUrl(PUBLIC_HOST)
    }
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return { pageProps }
  }

  componentDidMount() {
  }

  render() {
    const {
      Component, pageProps, store,
    } = this.props;

    return (
      <Provider store={store}>
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <script dangerouslySetInnerHTML={{
            __html: `
            const win = window;

            win.flex = () => {
              const doc = win.document;
            
              let metaEl = doc.querySelector('meta[name="viewport"]');
            
              if (!metaEl) {
                metaEl = doc.createElement('meta');
                metaEl.setAttribute('name', 'viewport');
                doc.head.appendChild(metaEl);
                metaEl.setAttribute('content', 'width=device-width,user-scalable=no');
              }
              const scale = 1;
            
              metaEl.setAttribute('content', 'width=device-width,user-scalable=no,initial-scale=' + scale + ',maximum-scale=' + scale + ',minimum-scale=' + scale);
              doc.documentElement.style.fontSize = ('50px');
            };
            
            win.flex();
          `,
          }}
          />
          <script src="https://cdn-static.farfetch-contents.com/assets/marketing-tracking-omnitrackingsdk/latest/omnitrackingsdk.min.js" async />
        </Head>
        <Component {...pageProps} />
      </Provider>
    )
  }
}

export default withRedux(initStore)(MyApp);
