import React from 'react'
import Head from 'next/head'

const Home = () => (
  <div className="container">
    <Head>
      <title>Create Next App</title>
    </Head>
    <div>Hello World!</div>
  </div>
)

export default Home
