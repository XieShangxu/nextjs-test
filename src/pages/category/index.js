import React, { Component } from 'react'
import { connect } from 'react-redux'
// import Router from 'next/router'
import cx from 'classnames'
import Head from 'next/head'
import { getContent } from '@@/actions/categoryAction'
import { cmsParse } from '@@/utils/cmsUtil'
import MainCategory from '@@/components/Category/MainCategory'
import AzBrand from '@@/components/Category/AzBrand'
import Tab from '@@/components/Category/Tab'
import styles from './style.less'

const componentMap = {
  ctgMain: MainCategory,
  azbrand: AzBrand,
};

class Category extends Component {
  static async getInitialProps({ store }) {
    // const {
    //   benefits, token, currency, userId,
    // } = query || {};
    const params = {
      'space-code': 'adslandingpage',
      codes: 'category-menu-women,category-menu-men,category-menu-kids',
    };

    // check if store has data
    const currentState = store.getState();
    const { category } = currentState
    if (Object.keys(category.contentData).length) {
      return { contentData: category.contentData }
    }

    const data = await store.dispatch(getContent(params));
    const homeContent = cmsParse(data);

    return { contentData: homeContent };
  }

  constructor(props) {
    super(props);
    this.state = {
      gender: 'women',
      tab: '',
    };
  }

  componentDidMount() {
    const { contentData } = this.props;
    const { gender } = this.state;
    const component = contentData[`category-menu-${gender}`][0];
    const tabName = Object.values(component)[0].name;

    this.setState({
      tab: tabName,
    })
    this._enableImgLazyLoading();
  }

  _enableImgLazyLoading() {
    if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window && 'intersectionRatio' in window.IntersectionObserverEntry.prototype) {
      this.enableLazyImg = true;
      // localStorage.setItem('enableLazyImg', true);
    }
    if (this.enableLazyImg) {
      const lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
      const lazyImageObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            const lazyImage = entry.target;

            lazyImage.src = lazyImage.dataset.src;
            lazyImageObserver.unobserve(lazyImage);
          }
        });
      });

      lazyImages.forEach((lazyImage) => {
        lazyImageObserver.observe(lazyImage);
        lazyImage.onload = () => {
          lazyImage.classList.remove('lazy');
        };
      });
    }
  }

  handleTabItemClick(key) {
    const { contentData } = this.props;
    const component = contentData[`category-menu-${key}`][0];
    const tabName = Object.values(component)[0].name;
    this.setState({
      gender: key,
      tab: tabName,
    })
  }

  handleSideTabClick(tab) {
    this.setState({
      tab,
    })
  }

  render() {
    const { contentData } = this.props;
    const { gender, tab } = this.state;
    const contents = contentData[`category-menu-${gender}`];
    const tabComponent = tab ? contents.find((c) => Object.values(c)[0].name === tab) : contents[0];
    const tabName = Object.keys(tabComponent)[0];
    const ComponentItem = componentMap[tabName];
    const tabContent = tabComponent[tabName];

    return (
      <div>
        <Head>
          <title>Category</title>
        </Head>
        <div className={styles['category-wrapper']}>
          <Tab onTabItemClick={(key) => this.handleTabItemClick(key)} />
          <div className={styles['sidebar-wrapper']}>
            {
              contents.map((com) => {
                // eslint-disable-next-line no-unused-vars
                const [[key, value]] = Object.entries(com);

                return (
                  // eslint-disable-next-line jsx-a11y/no-static-element-interactions
                  <div key={value.name} className={cx(styles['sidebar-item'], value.name === tab ? styles.active : '')} onClick={() => { this.handleSideTabClick(value.name) }}>{value.name}</div>
                )
              })
            }
          </div>
          <div className={styles['content-wrapper']}>
            {
              ComponentItem ? (
                <ComponentItem {...tabContent} gender={gender} />
              ) : null
            }
          </div>
        </div>
        <style jsx global>{`
          .lazy {
            display: block;
            min-height: 3rem;
            background: linear-gradient(to right, #f5f5f5 25%, #e9e9e9 50%, #f5f5f5 75%);
            background-size: 400% 100%;
            animation: gradientScroll 2s infinite;
            animation-timing-function: linear;
            transition: background-position 2s;
          }

          @keyframes gradientScroll {
            0% {
              background-position: 100% 0;
            }

            25% {
              background-position: 75% 0;
            }

            75% {
              background-position: 25% 0;
            }

            100% {
              background-position: 0 0;
            }
          }
        `}
        </style>
      </div>
    )
  }
}

export default connect((store) => (store))(Category);
