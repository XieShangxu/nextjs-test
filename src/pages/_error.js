import React from 'react'

function Error({ errorStatus }) {
  return (
    <div>
      { errorStatus }
    </div>
  )
}

Error.getInitialProps = ({ err }) => {
  let errorStatus

  if (err) {
    errorStatus = JSON.stringify(err)
  }
  return { errorStatus }
}

export default Error
