import React, { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import { getContent } from '@@/actions/contentAction'
import { cmsParse } from '@@/utils/cmsUtil'
import Text from '@@/components/Content/Text'
import QA from '@@/components/Content/QA'
import TextwithLink from '@@/components/Content/TextwithLink'
import SingleImage from '@@/components/Content/SingleImage'
import DividingLine from '@@/components/Content/DividingLine'
import ContentTitle from '@@/components/Content/ContentTitle'
import SlideTopic from '@@/components/Content/SlideTopic'
import ProductSet from '@@/components/Content/ProductsSet'
import ProductsWithCTA from '@@/components/Content/ProductsWithCTA'
import SlideProducts from '@@/components/Content/SlideProducts'
import CardCarousel from '@@/components/Content/CardCarousel'
import Outfit from '@@/components/Content/Outfit'

const componentMap = {
  text: Text,
  qa: QA,
  textWithLink: TextwithLink,
  singleImage: SingleImage,
  singleImageII: SingleImage,
  dividingLine: DividingLine,
  contentTitle: ContentTitle,
  slideTopic: SlideTopic,
  productSet: ProductSet,
  productsWithCTA: ProductsWithCTA,
  slideProducts: SlideProducts,
  cardCarouselList: CardCarousel,
  outfit: Outfit,
};

class Content extends Component {
  static async getInitialProps({ store, query, isServer }) {
    const { contentId, sessionKey } = query || {};
    const params = {
      'space-code': 'wechatstore',
      codes: contentId,
    };
    console.log('init porps!!!!', isServer)

    const data = await store.dispatch(getContent(params, sessionKey));
    const homeContent = cmsParse(data);

    return { contentData: homeContent[contentId] };
  }

  componentDidMount() {
    this._enableImgLazyLoading();
  }

  handleItemClick(...arg) {
    console.log(arg)
  }

  handleJump() {
    Router.push({
      pathname: '/content/10024000',
      query: {
        sessionKey: '11111111',
      },
    })
  }

  _enableImgLazyLoading() {
    if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window && 'intersectionRatio' in window.IntersectionObserverEntry.prototype) {
      this.enableLazyImg = true;
      // localStorage.setItem('enableLazyImg', true);
    }
    if (this.enableLazyImg) {
      const lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
      const lazyImageObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            const lazyImage = entry.target;

            lazyImage.src = lazyImage.dataset.src;
            lazyImageObserver.unobserve(lazyImage);
          }
        });
      });

      lazyImages.forEach((lazyImage) => {
        lazyImageObserver.observe(lazyImage);
        lazyImage.onload = () => {
          lazyImage.classList.remove('lazy');
        };
      });
    }
  }

  render() {
    const { contentData } = this.props;

    return (
      <div>
        <Head>
          <title>Content</title>
        </Head>
        {
          contentData.map((com, i) => {
            const [[key, value]] = Object.entries(com);
            const ComponentItem = componentMap[key];

            return ComponentItem ? (
              <ComponentItem
                key={key + i}
                comName={key}
                info={value}
                onItemClick={(...arg) => this.handleItemClick(arg)}
              />
            )
              : null
          })
        }
        <div role="button" onClick={() => this.handleJump()} tabIndex={0}>jump to ...</div>
        <style jsx global>{`
          .lazy {
            display: block;
            min-height: 3rem;
            background: linear-gradient(to right, #f5f5f5 25%, #e9e9e9 50%, #f5f5f5 75%);
            background-size: 400% 100%;
            animation: gradientScroll 2s infinite;
            animation-timing-function: linear;
            transition: background-position 2s;
          }

          @keyframes gradientScroll {
            0% {
              background-position: 100% 0;
            }

            25% {
              background-position: 75% 0;
            }

            75% {
              background-position: 25% 0;
            }

            100% {
              background-position: 0 0;
            }
          }
        `}
        </style>
      </div>
    )
  }
}

export default connect((store) => (store))(Content);
