import React, { Component } from 'react'
import { connect } from 'react-redux'
import Router from 'next/router'
import Head from 'next/head'
import classnames from 'classnames'
import SingleImage from '@@/components/Content/SingleImage'
import { formatTime } from '@@/utils/common'
import { setLoyaltyContent, initLoyaltyData } from '../../actions/loyaltyAction'
import styles from './loyalty.less'

const ModuleName = {
  myProfile: 'myProfile',
  myBenefits: 'myBenefits',
  upcomingBenefits: 'upcomingBenefits',
  myActivities: 'myActivities',
  loyaltyFaq: 'loyaltyFaq',
}
const TierMap = {
  'FFACCESS-Bronze': 'Bronze',
  'FFACCESS-Silver': 'Silver',
  'FFACCESS-Gold': 'Gold',
  'FFACCESS-Platinum': 'Platinum',
}

class Loyalty extends Component {
  static async getInitialProps({ store, query }) {
    const {
      benefits, token, currency, userId,
    } = query || {};
    const contentParams = {
      'space-code': 'wechatstore',
      codes: 'loyalty001',
      'target-benefits': benefits,
    };
    const pandaParams = {
      token: decodeURIComponent(token),
      currency,
      userId,
    }
    // check if store has data
    const currentState = store.getState();
    const { loyalty } = currentState
    if (Object.keys(loyalty.contentData).length) {
      return { contentData: loyalty.contentData, userAccessInfo: loyalty.userAccessInfo }
    }
    // end check
    const [
      userAccessInfo, contentData,
    ] = await store.dispatch(initLoyaltyData(pandaParams, contentParams))
    return { contentData, userAccessInfo };
  }

  componentDidMount() {
    // console.log(window.location)
    this._enableImgLazyLoading();
    const { contentData, userAccessInfo, dispatch } = this.props;
    dispatch(setLoyaltyContent({ contentData, userAccessInfo }));
  }

  handleJump() {
    Router.push({
      pathname: '/loyalty/ffAccess',
      query: {
        sessionKey: '11111111',
      },
    })
  }

  _enableImgLazyLoading() {
    if ('IntersectionObserver' in window && 'IntersectionObserverEntry' in window && 'intersectionRatio' in window.IntersectionObserverEntry.prototype) {
      this.enableLazyImg = true;
      // localStorage.setItem('enableLazyImg', true);
    }
    if (this.enableLazyImg) {
      const lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));
      const lazyImageObserver = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            const lazyImage = entry.target;

            lazyImage.src = lazyImage.dataset.src;
            lazyImageObserver.unobserve(lazyImage);
          }
        });
      });

      lazyImages.forEach((lazyImage) => {
        lazyImageObserver.observe(lazyImage);
        lazyImage.onload = () => {
          lazyImage.classList.remove('lazy');
        };
      });
    }
  }

  render() {
    console.log('render', this.props)
    const { contentData, userAccessInfo } = this.props;
    const { intro: benefitIntro, benefitList } = contentData.myBenefits || {}
    const { benefitList: upcomingBenefitList } = contentData.upcomingBenefits || {}
    const { activityCard: cardItemList } = contentData[ModuleName.myActivities] || {}
    const loyaltyFaq = contentData[ModuleName.loyaltyFaq] || {}
    const {
      currentValue, minimumValue, maximumValue, tier, startDate, endDate,
    } = userAccessInfo || {}
    const percentage = currentValue === 0 ? 0.1 : ((currentValue / maximumValue) * 100)
    const tierCardClassName = `bg-${TierMap[tier]}`

    return (
      <>
        <Head>
          <title>探索贵宾会籍</title>
        </Head>
        <div className={styles.wrapper}>
          <div className={styles.tierInfo}>
            <div className={styles.iconBox}>
              <div className={styles.iconAccess} />
              <div className={classnames(styles.iconTier, styles[tierCardClassName])} />
            </div>
            <div className={styles.line}>Michael.ke，恭喜您成为Farfetch用户已经365天了</div>
            <div className={styles.line}>{contentData[ModuleName.myProfile].intro.desc}</div>
          </div>
          {/* tier level card */}
          <div className={styles.title}>目前您的进程</div>
          <div className={classnames(styles.tierCard, styles[tierCardClassName])} role="button" onClick={() => this.handleJump()} tabIndex={-1}>
            <div className={styles.iconAccess} />
            <div className={classnames(styles.iconTier, styles[tierCardClassName])} />
            <div className={classnames(styles.line, styles.dateInfo)}>
              起始日:  {formatTime(new Date(startDate))}  |  截止日:  {formatTime(new Date(endDate))}
            </div>
            <div className={styles.line}>
              <div>{TierMap[tier]}</div>
              <div className={styles.tierLevel}>
                <div className={styles.barContainer} style={{ width: `${percentage}%` }}>
                  <div className={styles.bar} />
                </div>
              </div>
              <div className={styles.priceInfo}>
                <div>¥ {minimumValue}</div>
                <div>¥ {maximumValue}</div>
              </div>
            </div>
            <div className={styles.upgradeInfo}>
              截止2020年12月12日，还需消费￥8,000 升级铂金会员
              <div className={styles.iconArrow} />
            </div>
          </div>
          {/* benefit list */}
          <div className={styles.title}>您的贵宾礼遇</div>
          <div className={styles.benefitsWrapper}>
            <div className={styles.subTitle}>{benefitIntro}</div>
            <div className={styles.benefitList}>
              {
                (benefitList || []).map((item, i) => (
                  <div className={styles.item} key={i}>
                    <div className={styles.icon} style={{ backgroundImage: `url(${item.icon})` }} />
                    <div className={styles.text}>
                      {item.title}
                      <div className={styles.subText}>{item.subTitle}</div>
                    </div>
                    <div className={styles.iconArrow} />
                  </div>
                ))
              }
            </div>
          </div>
          {/* upcoming benefit list */}
          <div className={styles.upcomingBenefits}>
            <div className={styles.title}>您马上就能解锁这些礼遇了……</div>
            <div className={styles.subTitle}>铂金会员消费满¥50,000就能解锁以下礼遇</div>
            <div className={styles.benefitList}>
              {
                (upcomingBenefitList || []).map((item, i) => (
                  <div className={styles.item} key={i}>
                    <div className={styles.icon} style={{ backgroundImage: `url(${item.icon})` }} />
                    <div className={styles.text}>
                      {item.title}
                      <div className={styles.subText}>{item.subTitle}</div>
                    </div>
                  </div>
                ))
              }
            </div>
          </div>
          {/* My Activity */}
          <div className={styles.title}>专属活动</div>
          <div className={styles.activityList}>
            <div className={styles['card-list']}>
              {cardItemList.map((item) => {
                return (
                  <div key={item.image} className={styles['card-item']}>
                    <div className={styles['card-image']} style={item.image ? { backgroundImage: `url(${item.image})` } : {}} />
                    <div className={styles['card-title']}>{item.title}</div>
                    <div className={styles['card-subtitle']}>{item.subTitle}</div>
                  </div>
                )
              })}
            </div>
          </div>
          {/* Loyalty FAQ */}
          <div className={styles.title}>常见问题</div>
          <div className={styles.faqWrapper}>
            <SingleImage info={{ singleImage: loyaltyFaq.banner }} />
            <div className={styles.intro}>{loyaltyFaq.intro}</div>
          </div>
        </div>
        <style jsx global>{`
          .lazy {
            display: block;
            min-height: 3rem;
            background: linear-gradient(to right, #f5f5f5 25%, #e9e9e9 50%, #f5f5f5 75%);
            background-size: 400% 100%;
            animation: gradientScroll 2s infinite;
            animation-timing-function: linear;
            transition: background-position 2s;
          }

          @keyframes gradientScroll {
            0% {
              background-position: 100% 0;
            }

            25% {
              background-position: 75% 0;
            }

            75% {
              background-position: 25% 0;
            }

            100% {
              background-position: 0 0;
            }
          }
        `}
        </style>
      </>
    )
  }
}

export default connect((store) => ({
  store,
}))(Loyalty);
