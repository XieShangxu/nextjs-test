import React from 'react';
import classNames from 'classnames';
import styles from './index.less';

const QA = ({ info }) => {
  const { title, qaItems } = info;

  return (
    <div className={styles['qa-wrapper']}>
      {
        (title) ? (
          <p className={styles['qa-title']}>{title}</p>
        ) : null
      }
      <div className={styles['qa-detail']}>
        {
          qaItems.map((item, index) => (
            <div key={index} className={styles['qa-item-wrapper']}>
              <p className={classNames(styles['qa-item-title'], styles['text-eclips-lines'])}>{item.question}</p>
              <p className={classNames(styles['qa-item-content'], styles['text-eclips-lines'])}>{item.answer}</p>
            </div>
          ))
        }
      </div>
    </div>
  );
}

export default QA;
