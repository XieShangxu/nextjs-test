import React from 'react';
import classNames from 'classnames';
import currencyMap from '../../../../utils/currencyUtil';
import styles from './setItem.less';

const ProductSetItem = ({ product, imgTransparent, showPrice }) => {
  const productImg = product.images.length !== 0 && (product.images[0] || {}).url;
  const brandName = product.brand.name.toUpperCase();
  const { priceWithoutDiscount, price } = product;
  const isDiscount = price !== priceWithoutDiscount;
  const currency = sessionStorage.getItem('ff-currency') || 'CNY';

  return (
    <div className={classNames(styles['product-item'], imgTransparent ? styles['product-transpanret'] : '')}>
      <div className={styles['product-img']} style={productImg ? { backgroundImage: `url(${productImg})` } : {}} />
      <div className={styles['product-brand']}>{brandName}</div>
      <div className={styles['product-desc']}>{product.shortDescription}</div>
      {
        showPrice && (
          <div className={styles['price-box']}>
            <div className={classNames(styles['price-without-discount'], isDiscount ? styles['line-through'] : '')}>
              {currencyMap[currency]}{Math.round(priceWithoutDiscount)}
            </div>
            {
              isDiscount
              && <div className={styles.price}>{currencyMap[currency]}{Math.round(price)}</div>
            }
          </div>
        )
      }
    </div>
  );
}

export default ProductSetItem;
