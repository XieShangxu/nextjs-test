import React, { Component } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux'
import { getListBySetId } from '../../../actions/contentAction';
import styles from './productSet.less';
import ProductSetItem from './setItem';
import Spinner from '../../../shared/Components/Spinner';

class ProductSet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      isLoading: true,
      noData: false,
    };
  }

  componentDidMount() {
    const { info: { setId } } = this.props;

    if (setId) {
      this.getProductSet();
    }
  }

  getProductSet() {
    const { info: { setId, quantity }, dispatch } = this.props;
    const pageSize = (quantity && (quantity <= 20)) ? quantity : 20;

    const param = {
      'page-size': pageSize,
    };

    dispatch(getListBySetId({
      setId,
      param,
    })).then((res) => {
      const productList = res.productSummaries;

      if (!productList || productList.length < 2) {
        this.setState({
          isLoading: false,
          noData: true,
        });

        return;
      }

      this.setState({
        productList,
        isLoading: false,
      });
    });
  }

  handleProductClick(e, product, i) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(product, i);
    }
  }

  renderTitle() {
    const { info: { title, subTitle } } = this.props;

    if (title) {
      return (
        <div className={styles['product-set-title']}>
          {title}
          {
            subTitle && (
              <div className={styles['product-set-subTitle']}>
                {subTitle}
              </div>
            )
          }
        </div>
      );
    }

    return null;
  }

  renderProductList() {
    const { productList } = this.state;
    let list;

    if (productList.length % 2 !== 0) {
      list = productList.slice(0, -1);
    } else {
      list = productList;
    }

    return (
      <ul className={styles['product-list-wrap']}>
        {
          list.map((product, i) => (
            <li
              className={styles['product-item-wrap']}
              key={product.id}
              onClick={(e) => { this.handleProductClick(e, product, i); }}
            >
              <ProductSetItem product={product} />
            </li>
          ))
        }
      </ul>
    );
  }

  render() {
    const { isLoading, noData } = this.state;
    const productSetClass = cx(styles['product-set-container'], isLoading && styles['product-set-isloading']);

    return !noData ? (
      <div className={productSetClass}>
        {
          isLoading ? <Spinner type="plp" full={false} />
            : (
              <div>
                {this.renderTitle()}
                {this.renderProductList()}
              </div>
            )
        }
      </div>
    ) : null;
  }
}

export default connect(() => ({}))(ProductSet);
