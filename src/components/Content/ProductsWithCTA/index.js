import React, { Component } from 'react';
import { connect } from 'react-redux'
import styles from './productsWithCTA.less';
import ProductSetItem from '../ProductsSet/setItem';
import { getListBySetId, getProductsBySearch } from '../../../actions/contentAction';

class ProductsWithCTA extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };

    this.handleBtnClick = this.handleBtnClick.bind(this);
  }

  componentDidMount() {
    const { dispatch, info: { contextLink } } = this.props;
    const param = {
      ...contextLink,
      'page-size': 4,
    };

    if (param.set) {
      if (param.ctg) {
        param.category = param.ctg;
        delete param.ctg;
      }
      dispatch(getListBySetId({
        setId: contextLink.set,
        param,
      })).then((res) => {
        const products = res.productSummaries;

        this.setState({
          products,
        });
      });
    } else {
      dispatch(getProductsBySearch(param)).then((data) => {
        const products = data.productSummaries;

        this.setState({
          products,
        });
      });
    }
  }

  handleBtnClick(e) {
    e.stopPropagation();
    const { onItemClick, info: { contextLink } } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(contextLink);
    }
  }

  handleProductClick(e, product, i) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(product, i);
    }
  }

  render() {
    const { info: { title, btnName } } = this.props;
    const { products } = this.state;

    return (
      <div className={styles.wrapper}>
        <div className={styles.title}>{title}</div>
        <ul className={styles['product-list-wrap']}>
          {
            products.map((product, i) => (
              <li
                className={styles['product-item-wrap']}
                key={product.id}
                onClick={(e) => { this.handleProductClick(e, product, i); }}
              >
                <ProductSetItem product={product} showPrice />
              </li>
            ))
          }
        </ul>
        {
          btnName && <div className={styles['see-all']} onClick={this.handleBtnClick} role="button" tabIndex="0">{btnName}</div>
        }
      </div>
    );
  }
}

export default connect(() => ({}))(ProductsWithCTA);
