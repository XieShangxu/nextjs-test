import React, { Component } from 'react';
import styles from './slideTopic.less';
import SlideTopicItem from './SlideTopicItem';

class SlideTopic extends Component {
  handleTopicClick(e, contextLink, index) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(contextLink, index)
    }
  }

  render() {
    let { info: { cardItemList } } = this.props;
    if (cardItemList.length > 10) { cardItemList = cardItemList.slice(0, 10); }

    return (
      <div className={styles['slide-topic-container']}>
        <ul className={styles['slide-topic']}>
          {
            cardItemList.map((card, index) => (
              <li
                className={styles['slide-topic-itemwrap']}
                key={index}
                onClick={(e) => { this.handleTopicClick(e, card.contextLink, index); }}
              >
                <SlideTopicItem
                  title={card.title}
                  subTitle={card.subTitle}
                  image={card.image}
                />
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

export default SlideTopic;
