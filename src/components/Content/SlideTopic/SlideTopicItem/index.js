import React from 'react';
import styles from './slideTopicItem.less';

const SlideTopicItem = ({ title, image, subTitle }) => {
  return (
    <div className={styles['slide-topic-item']}>
      <div className={styles['slide-topic-img']} style={image ? { backgroundImage: `url(${image})` } : {}} />
      <div className={styles['slide-topic-title']}>{title}</div>
      <div className={styles['slide-topic-subtitle']}>{subTitle}</div>
    </div>
  );
}

export default SlideTopicItem;
