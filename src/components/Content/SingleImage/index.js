import React from 'react';
import classNames from 'classnames';
import style from './singleImage.less';

export default function SingleImage({ info, comName, onItemClick }) {
  const { singleImage: imgUrl, contextLink } = info || {};
  const enableLazyImg = true;
  const singleImageII = comName === 'singleImageII';

  return (
    <div
      className={classNames(style.wrapper, singleImageII ? style.singleImageII : '')}
      onClick={() => onItemClick(contextLink)}
      role="button"
      tabIndex="0"
    >
      <img className={classNames(style.img, enableLazyImg ? 'lazy' : '')} src={enableLazyImg ? '' : imgUrl} data-src={imgUrl} alt="" />
    </div>
  );
}
