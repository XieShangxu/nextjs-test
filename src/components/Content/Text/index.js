import React from 'react';
import { parseMultiLine } from '../../../utils/cmsUtil';
import style from './index.less';

export default function Text({ info }) {
  const { title = {}, content = {} } = info || {};
  const newTitle = { ...title, text: parseMultiLine(title.text || '') };
  const newContent = { ...content, text: parseMultiLine(content.text || '') };

  return (
    <div className={style.wrapper}>
      {
        newTitle.text.map((line) => (
          <div
            key={line}
            className={style.title}
            style={{ textAlign: title.displayOptions && title.displayOptions.align }}
          >
            {line}
          </div>
        ))
      }
      {
        newContent.text.map((line) => (
          <div
            key={line}
            className={style.content}
            style={{ textAlign: content.displayOptions && content.displayOptions.align }}
          >
            {line}
          </div>
        ))
      }
    </div>
  );
}
