import React from 'react';
import style from './contentTitle.less';

const ContentTitle = ({ info }) => {
  const { title, author, topic } = info;

  return (
    <div className={style.wrapper}>
      <div className={style.title}>{title}</div>
      <div className={style.author}>{author}</div>
      <div className={style.topic}>{topic}</div>
    </div>
  );
};

export default ContentTitle;
