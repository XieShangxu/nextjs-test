import React, { Component } from 'react';
import cx from 'classnames';
import styles from './index.less';

class TextWithLink extends Component {
  getTextList() {
    const { info: { texts } } = this.props;
    const list = texts.length > 10 ? texts.slice(0, 10) : texts;
    const textList = list.map((text, index) => {
      const { context } = text;
      const textClass = cx(
        styles['textwithlink-text-item'],
        (context.pdpLink.productId || context.plpLink.typeId || context.contentLink.contentId) && styles['text-underline'],
      );

      return (
        <li
          className={textClass}
          key={index}
          onClick={(e) => this.handleTextClick(e, text.context, index)}
        >
          {text.text}
        </li>
      );
    });

    return textList;
  }

  handleTextClick(e, context, i) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(context, i);
    }
  }

  render() {
    const { info } = this.props;
    const { title } = info;

    return (
      <div className={styles['textwithlink-container']}>
        <div className={styles['textwithlink-title']}>{title}</div>
        <ul className={styles['textwithlink-text-list']}>
          {this.getTextList()}
        </ul>
      </div>
    );
  }
}

export default TextWithLink;
