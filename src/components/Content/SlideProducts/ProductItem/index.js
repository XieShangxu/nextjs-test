import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './productItem.less';

const SlideProductsItem = ({ product }) => {
  const productImg = product.images.length !== 0 && product.images[0].url;
  const brandName = product.brand.name.toUpperCase();
  const { shortDescription } = product;

  return (
    <div className={styles['slide-products-item']}>
      <div className={styles['slide-products-img']} style={productImg ? { backgroundImage: `url(${productImg})` } : {}} />
      <div className={styles['slide-products-brand']}>{brandName}</div>
      <div className={styles['slide-products-desc']}>{shortDescription}</div>
    </div>
  );
}

SlideProductsItem.propTypes = {
  product: PropTypes.shape({}).isRequired,
};

export default SlideProductsItem;
