import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import cx from 'classnames';
import styles from './slideProducts.less';
import SlideProductsItem from './ProductItem';
import { getListBySetId } from '../../../actions/contentAction';
import Spinner from '../../../shared/Components/Spinner';

class SlideProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      isLoading: true,
      noData: false,
    };
  }

  componentDidMount() {
    const { info: { setId } } = this.props;

    if (setId) {
      this.getSlideProducts();
    }
  }

  getSlideProducts() {
    const { dispatch, info: { setId, quantity } } = this.props;
    const pageSize = (quantity && (quantity <= 10)) ? quantity : 10;

    const param = {
      'page-size': pageSize,
    };

    dispatch(getListBySetId({
      setId,
      param,
    })).then((res) => {
      const productList = res.productSummaries;

      if (!productList || productList.length < 2) {
        this.setState({
          isLoading: false,
          noData: true,
        });

        return;
      }

      this.setState({
        productList,
        isLoading: false,
      });
    });
  }

  handleBtnClick(e, btnContext) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(btnContext);
    }
  }

  handleProductClick(e, product, i) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(product, i);
    }
  }

  render() {
    const {
      info: {
        title, subTitle, btnName, btnContext,
      },
    } = this.props;
    const { productList, noData, isLoading } = this.state;
    const productSetClass = cx(styles['slide-products-container'], isLoading && styles['slide-products-isloading']);

    return !noData ? (
      <div className={productSetClass}>
        {
          isLoading
            ? <Spinner type="slideproducts" full={false} />
            : (
              <div>
                <div className={styles['slide-products-title']}>{title}</div>
                {
                  subTitle && <div className={styles['slide-products-subtitle']}>{subTitle}</div>
                }
                <div className={styles['slide-products-list']}>
                  <ul className={styles['slide-products']}>
                    {
                      productList.map((p, index) => (
                        <li
                          className={styles['slide-products-itemwrap']}
                          key={index}
                          onClick={(e) => { this.handleProductClick(e, p, index); }}
                        >
                          <SlideProductsItem product={p} />
                        </li>
                      ))
                    }
                  </ul>
                </div>
                {
                  btnName
                  && btnContext
                  && (
                    <button
                      className={styles['slide-products-btn']}
                      onClick={(e) => { this.handleBtnClick(e, btnContext); }}
                      type="button"
                    >
                      {btnName}
                    </button>
                  )
                }
              </div>
            )
        }
      </div>
    ) : null;
  }
}

SlideProducts.propTypes = {
  info: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subTitle: PropTypes.string,
    setId: PropTypes.number.isRequired,
    quantity: PropTypes.number,
    btnName: PropTypes.string,
    btnContext: PropTypes.objectOf(PropTypes.object),
  }),
  onItemClick: PropTypes.func,
};

export default connect()(SlideProducts);
