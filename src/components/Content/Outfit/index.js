import React, { Component } from 'react';
import classNames from 'classnames';
import { connect } from 'react-redux'
import style from './outfit.less';
import ProductSetItem from '../ProductsSet/setItem';
import { getProductById } from '../../../actions/contentAction';

class Outfit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    const { info: { productId } } = this.props;

    this.loadProducts(productId.map((product) => product.productId));
  }

  loadProducts(ids) {
    const { dispatch } = this.props;

    Promise.all(ids.map((id) => dispatch(getProductById({ id })))).then((resps) => {
      const productSummaries = resps.map((data) => ({
        id: data.id,
        brand: {
          name: data.brandName,
        },
        shortDescription: data.shortDescription,
        images: data.images,
      }));

      this.setState({
        products: productSummaries,
      });
    });
  }

  handleProductClick(e, product, i) {
    e.stopPropagation();
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(product, i);
    }
  }

  render() {
    const { info: { image, title } } = this.props;
    const { products } = this.state;
    const enableLazyImg = true;

    return (
      <div className={style.wrapper}>
        <div className={style.title}>{title}</div>
        <div className={classNames(style['image-box'])}>
          <img alt="" src={enableLazyImg ? '' : image} data-src={image} className={enableLazyImg ? 'lazy' : ''} />
        </div>
        <ul className={style['product-list-wrap']}>
          {
            products.map((product, i) => (
              <li
                className={style['product-item-wrap']}
                key={product.id}
                onClick={(e) => { this.handleProductClick(e, product, i); }}
              >
                <ProductSetItem product={product} imgTransparent />
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

export default connect(() => ({}))(Outfit);
