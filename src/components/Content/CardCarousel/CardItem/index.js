import React, { Component } from 'react';
import style from './cardItem.less';

export default class CardCarouselItem extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { onClick, contextLink } = this.props;

    if (typeof onClick === 'function') {
      onClick(contextLink);
    }
  }

  render() {
    const {
      image, title, subTitle, showTitle, showSubTitle,
    } = this.props;

    return (
      <div className={style.wrapper} onClick={this.handleClick} role="button" tabIndex="0">
        <img className={style['product-img']} src={image} alt="" />
        {
          showTitle && <div className={style.title}>{title}</div>
        }
        {
          showSubTitle && <div className={style.subtitle}>{subTitle}</div>
        }
      </div>
    );
  }
}
