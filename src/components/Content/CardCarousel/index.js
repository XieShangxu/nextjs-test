import React, { Component } from 'react';
import SwipeableViews from 'react-swipeable-views';
import CardCarouselItem from './CardItem';

class CardCarousel extends Component {
  constructor(props) {
    super(props);
    this.onCardClick = this.onCardClick.bind(this);
  }

  onCardClick(contextLink, i) {
    const { onItemClick } = this.props;

    if (typeof onItemClick === 'function') {
      onItemClick(contextLink, i);
    }
  }

  render() {
    const { info: { cardItemList = [] } } = this.props;
    const showCardTitle = !!cardItemList[0].title;
    const showCardSubTitle = !!cardItemList[0].subTitle;

    const swiperConfig = {
      root: {
        padding: '0 0.62rem',
        marginBottom: '0.48rem',
      },
      index: 1,
    };

    return (
      <SwipeableViews index={swiperConfig.index} style={swiperConfig.root}>
        {
          cardItemList.map((card, i) => (
            <CardCarouselItem
              key={i}
              onClick={(contextLink) => this.onCardClick(contextLink, i)}
              {...card}
              showTitle={showCardTitle}
              showSubTitle={showCardSubTitle}
            />
          ))
        }
      </SwipeableViews>
    );
  }
}

export default CardCarousel;
