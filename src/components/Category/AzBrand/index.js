import React, { Component } from 'react';
import cx from 'classnames';
import { connect } from 'react-redux'
import { getAzBrands, setAzBrands } from '../../../actions/categoryAction';
import styles from './azBrand.less'

class AzBrand extends Component {
  componentDidMount() {
    const { dispatch, azBrands } = this.props;

    if (!Object.keys(azBrands).length) {
      dispatch(getAzBrands({})).then((resp) => {
        dispatch(setAzBrands(resp));
      })
    }
  }

  renderGroupItems(brands) {
    return brands.map((brand) => {
      return (
        <div key={brand.id} className={styles['brand-item']}>
          <div className={styles.name}>{brand.name}</div>
          <div className={cx(styles.fav)} />
        </div>
      )
    })
  }

  render() {
    const { azBrands, gender } = this.props;
    const brandGroups = azBrands[gender] || [];

    return (
      <div className={styles['brands-wrapper']}>
        <div className={styles['fav-toggle']}>只看我喜欢的设计师</div>
        <div className={styles['brand-groups']}>
          { brandGroups.map((group) => {
            return (
              <div key={group.name} className={styles.group}>
                <div className={styles['group-header']}>{group.name.toUpperCase()}</div>
                { this.renderGroupItems(group.children) }
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  azBrands: state.category.azBrands,
});

export default connect(mapStateToProps)(AzBrand);
