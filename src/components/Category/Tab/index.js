import React, { Component } from 'react'
import cx from 'classnames'
import { connect } from 'react-redux'
import { PropTypes } from 'prop-types'
import styles from './tab.less'

const tabs = [{
  key: 'women',
  val: '女士',
}, {
  key: 'men',
  val: '男士',
}, {
  key: 'kids',
  val: '儿童',
}]

class Tab extends Component {
  constructor(props) {
    super(props);
    this.handleTabClick = this.handleTabClick.bind(this);
    this.state = {
      currentKey: 'women',
    };
  }

  componentDidMount() {
  }

  handleTabClick(key) {
    const { onTabItemClick } = this.props;
    const { currentKey } = this.state;
    if (currentKey === key) return;
    this.setState({
      currentKey: key,
    })
    onTabItemClick(key);
  }

  render() {
    const { currentKey } = this.state;
    return (
      <div className={styles['tab-wrapper']}>
        { tabs.map((tab) => {
          return (
            // eslint-disable-next-line jsx-a11y/no-static-element-interactions
            <div key={tab.key} className={cx(styles['tab-item'], currentKey === tab.key ? styles.active : '')} onClick={() => this.handleTabClick(tab.key)}>{tab.val}</div>
          )
        })}
      </div>
    )
  }
}

Tab.propTypes = {
  onTabItemClick: PropTypes.func,
};

export default connect(() => ({}))(Tab);
