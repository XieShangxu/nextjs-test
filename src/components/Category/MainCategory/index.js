import React, { Component } from 'react';
import { connect } from 'react-redux'
import callAppFunction from '@@/utils/protocol'
import styles from './mainCategory.less'

class MainCategory extends Component {
  componentDidMount() {
  }

  handleItemClick(url) {
    const params = {
      path: url,
    }
    callAppFunction('navigation', params)
  }

  render() {
    const { items } = this.props;

    return (
      <div className={styles['main-ctg-wrapper']}>
        { items.map((item) => {
          return (
            // eslint-disable-next-line jsx-a11y/no-static-element-interactions
            <div key={item.image} className={styles['main-item']} style={{ backgroundImage: `url(${item.image})` }} onClick={() => { this.handleItemClick(item.url) }}>
              <div className={styles.desc}>
                <div className={styles.title}>{item.title}</div>
                <div className={styles.subtitle}>{item.subtitle}</div>
              </div>
            </div>
          )
        })}
      </div>
    )
  }
}

export default connect(() => ({}))(MainCategory);
