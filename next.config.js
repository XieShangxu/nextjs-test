const withLess = require('@zeit/next-less')
// eslint-disable-next-line import/no-extraneous-dependencies
const path = require('path')
// const cfg = require('config')

module.exports = withLess({
  webpack: (config, { isServer }) => {
    config.module.rules.push({
      enforce: 'pre',
      test: /.(js|jsx)$/,
      loader: 'eslint-loader',
      exclude: [
        path.resolve(__dirname, '/node_modules'),
      ],
    }, {
      test: /\.svg/,
      use: {
        loader: 'svg-url-loader',
        options: {},
      },
    }, {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      use: [
        {
          loader: 'file-loader',
          options: {
            limit: 8192,
            publicPath: '/_next/static/chunks/fonts/',
            outputPath: `${isServer ? '../' : ''}static/chunks/fonts/`,
            name: '[name]-[hash].[ext]',
          },
        },
      ],
    })

    return config
  },
  cssModules: true,
  cssLoaderOptions: {
    importLoaders: 1,
    localIdentName: '[local]___[hash:base64:5]',
  },
  // assetPrefix: '/page',
})
