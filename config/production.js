const config = {
  env: 'production',
  apiHost: 'https://api.ff-svc.cn/westore',
  assetPrefix: '/page',
}

module.exports = config
