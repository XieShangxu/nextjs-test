const express = require('express')
const next = require('next')

const port = parseInt(process.env.PORT, 10) || 3001
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const winston = require('winston');

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    //
    // - Write all logs with level `error` and below to `error.log`
    // - Write all logs with level `info` and below to `combined.log`
    //
    new winston.transports.File({ filename: './logs/ssr.log', level: 'info' }),
  ],
});

const config = require('config');

const ffdev = config.get('env') === 'development';

//
// If we're not in production then log to the `console` with the format:
// `${info.level}: ${info.message} JSON.stringify({ ...rest }) `
//
if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

app.prepare().then(() => {
  const server = express()
  const prefix = config.get('assetPrefix')
  app.setAssetPrefix(prefix || '/')

  if (prefix) {
    server.use('/fonts/', async (req, res) => {
      const newURL = req.originalUrl.replace('/fonts', `${prefix}/fonts`)
      res.redirect(newURL)
    })
  }

  server.get('/content/:id', async (req, res) => {
    const { params: { id }, query: { sessionKey } } = req
    const actualPage = '/content'
    const queryParams = {
      contentId: id,
      sessionKey,
      ffdev,
    }
    logger.info(req.url)
    app.render(req, res, actualPage, queryParams)
  })

  server.get('/monitoring/ping', (req, res) => {
    res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload');
    res.send('ok')
  });
  server.get('/monitoring/pong', (req, res) => {
    res.setHeader('Strict-Transport-Security', 'max-age=31536000; includeSubDomains; preload');
    const result = {};

    try {
      result.uptime = process.uptime();
      result.memory = process.memoryUsage();
      result.isHealthy = true;
    } catch (e) {
      result.isHealthy = false;
      res.status(503);
    }

    res.json(result);
  });

  server.all('*', (req, res) => {
    if (ffdev) {
      req.query.ffdev = true
    }
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
}).catch((ex) => {
  console.error(ex.stack)
  process.exit(1)
})
